# Sarahouse Framework

## Introduction

Sarahouse is a Go-based framework complete with features like:
- Dependency Injection
- ORM & Pagination Module
- Model - Controller - Service architecture
- Rate Limits (default 100 requests/second)
- Middlewares (incl. Auth Middleware)
- Crontab Schedules
- Routers
- .env Processor
- etc.

## Directory Structure
**Click this link to see the directory tree**
https://tree.nathanfriend.io/?s=(%27options!(%27fancy!true~fullPath!false~trailingSlash!true~rootDot!false)~3(%273%27sarahouse2--api*controller4crontab0dep4func4interfa5iddleware0servi5odule.go*routes2%20%27)~version!%271%27)*2----%20%200*model42%5Cn3source!4s*5ce4m%0154320-*
## Dependency Injection
Services (the one that has access to DB layer) is encapsulated in an interface, of which will be called by a controller.

**api/interfaces/orderServiceInterface,go**

    type  OrderServiceInterface  interface {
		    GetAll(req definitions.PaginationRequest) (definitions.PaginationResult, error)
		    Create(order models.Order, productList string) (definitions.GenericCreationMessage, error)
		    GetCSVReport(custId int) (definitions.GenericAPIMessage, error)
}

**api/module.go**

    func (module *Module) Configure(injector *dingo.Injector) {web.BindRoutes(injector, new(Routes)) {
		    injector.Bind(new(interfaces.ProductServiceInterface)).To(services.ProductService{})
		    injector.Bind(new(interfaces.OrderServiceInterface)).To(services.OrderService{})
    
 }

**api/controllers/orderController.go**

    type (
	    OrderController struct {
		    responder *web.Responder
		    customValidator *validator.CustomValidator
		    orderService interfaces.OrderServiceInterface
		  }
		)
		
		func (orderController *OrderController) Inject(
			responder *web.Responder,
			customValidator *validator.CustomValidator,
			orderService interfaces.OrderServiceInterface,
		) {
		orderController.responder  = responder
		orderController.customValidator  = customValidator
		orderController.orderService  = orderService
		}

## API Auth

This framework is using basic authentication. You just need to provide "Username" and "Password" to the Request Header. The system will detect automatically whether you are a customer or administrator. 

## Examples

Routes list:
- GET /order-all : Get all orders.  Add "?customer_id=<random_integer>" to view only orders for that customer. If you already provided customer's Username and Password in the Request Header, then you don't need to provide this, and the list will be populated with orders of customer with id = customer_id.
- POST /order : Create new order.  You can provide these details in the request body:
	- CustomerID: int
	- Products: string (example: "15,25". These are product IDs from "product" table. you can put single and multiple product ids as long as they are separated with a comma like the example above.) If you already provided customer's Username and Password in the Request Header, then you don't need to provide this, and the order will be created automatically for customer with id = customer_id.
- GET /order-export: Export the order data of a customer in csv format.  If you already provided customer's Username and Password in the Request Header, then you don't need to provide this, and the order will be created automatically for customer with id = customer_id.

    

